package com.example.springboot_hw_001;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

@RestController
@RequestMapping("/api/v1/customers")
public class Controller {


    //Create ArrayList
    ArrayList<Customer> cusList = new ArrayList<>(){
        {
            add(new Customer("Sak Zin","Male",18,"Phnom Penh"));
            add(new Customer("XiLing","Female",19,"BattamBang"));
        }
    };
    //Insert
    @PostMapping("/")
    public ResponseEntity<ResponseMassage<Customer>> inserted(@RequestBody CustomerRequest customerRequest) {

        cusList.add(new Customer(customerRequest.getName(),customerRequest.getGender(),customerRequest.getAge(),customerRequest.getAddress()));


        return ResponseEntity.ok(new ResponseMassage<Customer>( "This record has Insert successfully",new Customer(cusList.size(), customerRequest.getName(),customerRequest.getGender(),customerRequest.getAge(),customerRequest.getAddress()),HttpStatus.OK,LocalDateTime.now()));
    }

    //Read All Customer
    @GetMapping("/")
    public ResponseEntity<ResponseMassage<ArrayList<Customer>>> readAll() {
        ResponseMassage responseMassage = new ResponseMassage<ArrayList<Customer>>();
        responseMassage.setMessage("All Record had found successfully");
        responseMassage.setCustomer(cusList);
        responseMassage.setStatus(HttpStatus.OK);
        responseMassage.setTime(LocalDateTime.now());
        return ResponseEntity.ok().body(responseMassage);
    }

    //Read By Name
    @GetMapping("/search")
    public ResponseEntity<ResponseMassage<Customer>> search (@RequestParam String name) {
        ResponseMassage responseMassage = new ResponseMassage<Customer>();
        responseMassage.setMessage("This record has found by Name Successfully");
        Boolean isFound = false;
        for (int i = 0; i < cusList.size(); i++) {
            if(cusList.get(i).getName().equalsIgnoreCase(name)){
                responseMassage.setCustomer(cusList.get(i));
                isFound = true;
                break;
            }
        }
        if(!isFound){
            return ResponseEntity.notFound().build();
        }
        responseMassage.setStatus(HttpStatus.OK);
        responseMassage.setTime(LocalDateTime.now());
        return ResponseEntity.ok().body(responseMassage);
    }

    //Read Customer Using ID
    @GetMapping("/{id}")
    public ResponseEntity<ResponseMassage<Customer>> getID(@PathVariable int id) {
        ResponseMassage responseMassage = new ResponseMassage<Customer>();
        responseMassage.setMessage("This record has found successfully");
        Boolean isFound = false;
        for (int i = 0; i < cusList.size(); i++) {
            if(id == cusList.get(i).getId()){
                isFound = true;
                break;
            }
        }
        if(!isFound){
            return ResponseEntity.notFound().build();
        }
        responseMassage.setCustomer(cusList.get(id-1));
        responseMassage.setStatus(HttpStatus.OK);
        responseMassage.setTime(LocalDateTime.now());
        return ResponseEntity.ok().body(responseMassage);
    }

    //Update Customer By ID
    @PutMapping("/{id}")
    @ResponseBody
    @ApiResponse(
            responseCode = "200",
            description = "OK",
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = ResponseMassage.class),
                            examples = @ExampleObject(value = "{\"message\": \"String\", \"customer\": {}, \"status\": \"100 Continue\", \"time\": \"2023-03-04T17:01:31.6517702\"}")
                    )
            }
    )
    public ResponseEntity<ResponseMassage<Customer>> UpdateID(@PathVariable int id,
                                                              @RequestBody CustomerRequest customerRequest) {
        ResponseMassage responseMassage = new ResponseMassage<Customer>();
        Boolean isFound = false;
        for (int i = 0; i < cusList.size(); i++) {
            if(id == cusList.get(i).getId()){
                cusList.set(id-1,new Customer(id,customerRequest.getName(),customerRequest.getGender(),customerRequest.getAge(),customerRequest.getAddress()));
                responseMassage.setMessage("This record Update Successfully");
                responseMassage.setCustomer(cusList.get(id-1));
                responseMassage.setStatus(HttpStatus.OK);
                responseMassage.setTime(LocalDateTime.now());
                isFound = true;
                break;
            }
        }
        if(!isFound){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(responseMassage);
    }

    //Delete Customer By ID
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseMassageDelete<Customer>> deleteID(@PathVariable int id) {
        ResponseMassageDelete responseMassage = new ResponseMassageDelete();
        Boolean isFound = false;
        for (int i = 0; i < cusList.size(); i++) {
            if(id == cusList.get(i).getId()){
                isFound = true;
                break;
            }
        }
        if(!isFound){
            return ResponseEntity.notFound().build();
        }
            cusList.remove(id-1);
            responseMassage.setMessage("Congratulation your delete is successfully");
            responseMassage.setStatus(HttpStatus.OK);
            responseMassage.setTime(LocalDateTime.now());
            return ResponseEntity.ok().body(responseMassage);
    }

}
