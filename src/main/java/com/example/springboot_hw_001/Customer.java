package com.example.springboot_hw_001;

public class Customer {
    public static int increment = 0;
    public int id,age;
    public String name,gender,address;

    public Customer(String name,String gender,int age, String address) {
        this.id = ++increment;
        this.age = age;
        this.name = name;
        this.gender = gender;
        this.address = address;
    }
    public Customer(int id,String name,String gender,int age, String address) {
        this.id = id;
        this.age = age;
        this.name = name;
        this.gender = gender;
        this.address = address;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    public Customer() {

    }


}
