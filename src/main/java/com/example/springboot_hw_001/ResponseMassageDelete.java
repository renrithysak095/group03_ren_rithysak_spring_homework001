package com.example.springboot_hw_001;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class ResponseMassageDelete<T>{

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;
    private HttpStatus status;
    private LocalDateTime time;



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public ResponseMassageDelete() {
    }

    public ResponseMassageDelete(String message, T customer, HttpStatus status, LocalDateTime time) {
        this.message = message;
        this.status = status;
        this.time = time;
    }
}
