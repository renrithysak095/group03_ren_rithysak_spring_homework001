package com.example.springboot_hw_001;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public class ResponseMassage<T>{

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;
    private T customer;
    private HttpStatus status;
    private LocalDateTime time;



    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getCustomer() {
        return customer;
    }

    public void setCustomer(T customer) {
        this.customer = customer;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }



    public ResponseMassage(String message, T customer, HttpStatus status, LocalDateTime time) {
        this.message = message;
        this.customer = customer;
        this.status = status;
        this.time = time;
    }

    public ResponseMassage(String message, HttpStatus status, LocalDateTime time) {
        this.message = message;
        this.status = status;
        this.time = time;
    }

    public ResponseMassage() {
    }
}
