package com.example.springboot_hw_001;

public class CustomerRequest {

    public int age;
    public String name,gender,address;

    public CustomerRequest(String name, String gender, int age, String address) {
        this.age = age;
        this.name = name;
        this.gender = gender;
        this.address = address;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    public CustomerRequest() {
    }


}
